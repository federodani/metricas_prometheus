import requests
import logging
from prometheus_client import Gauge, Counter

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

class Metrica(object):
	"""Definición de Metricas"""
	def __init__(self, host='', puerto=''):
		super(Metrica, self).__init__()		
		self.log = logging.getLogger()
		self.host = str(host)
		self.puerto = str(puerto)
		self.url = self.get_URL()
	
	def get_URL(self):
		self.log.debug('Obteniendo URL...')
		return 'http://'+self.host+':'+self.puerto+'/'
	
	def isalive(self):
		self.log.debug('Consultando estado API para '+ self.url)
		url_request =self.url+'isalive'
		metrica_isalive = 0
		status = Gauge('py_status_api_raspberry', 'Estado del servicio de la Api para raspberry pi', ['host', 'puerto'])
		ok = Counter('py_status_in_api_raspberry', 'Cantidad OK del Estado del servicio de la Api para raspberry pi', ['host', 'puerto'])
		fail = Counter('py_status_out_api_raspberry', 'Cantidad FALLA del Estado del servicio de la Api para raspberry pi', ['host', 'puerto'])		
		try:
			r = requests.get(url=url_request)
			self.log.debug('codigo:'+str(r.status_code) +' '+str(r.json()))
			if r.status_code == 200 and r.json() == True:
				metrica_isalive = 1
				ok.labels(host=self.host, puerto=self.puerto).inc()
		except requests.exceptions.HTTPError as errh:
		    self.log.error("Http Error:"+ str(errh))
		    fail.labels(host=self.host, puerto=self.puerto).inc()
		except requests.exceptions.ConnectionError as errc:
		    self.log.error("Error Conección:"+ str(errc))
		    fail.labels(host=self.host, puerto=self.puerto).inc()
		except requests.exceptions.Timeout as errt:
		    self.log.error("Timeout Error:"+ str(errt))
		    fail.labels(host=self.host, puerto=self.puerto).inc()
		except requests.exceptions.RequestException as err:
		    self.log.error("Otro Error: " + str(err))
		    fail.labels(host=self.host, puerto=self.puerto).inc()
		self.log.info('URL: ' + url_request +' Estado:' + str(metrica_isalive))
		status.labels(host=self.host, puerto=self.puerto).set(metrica_isalive)
		return metrica_isalive