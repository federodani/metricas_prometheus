#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import logging
import configparser
import os

from prometheus_client import Counter, Gauge

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

status = Gauge('py_status_api_raspberry', 'Estado del servicio de la Api para raspberry pi', ['host', 'puerto'])
ok = Counter('py_status_in_api_raspberry', 'Cantidad OK del Estado del servicio de la Api para raspberry pi',
             ['host', 'puerto'])
fail = Counter('py_status_out_api_raspberry', 'Cantidad FALLA del Estado del servicio de la Api para raspberry pi',
               ['host', 'puerto'])


def get_url(equipo, puerto):
    """
    Genera URL a partir de la ip del equipo y puerto
    :param equipo: Str
    :param puerto: Int
    :return: Str
    """
    return 'http://' + str(equipo) + ':' + str(puerto) + '/'


def get_status(equipo):
    """
    Obtiene el estado del servicio API corriendo en un equipo. 0= No activo, 1 = Activo
    :param equipo: Dict -> {'ip':unaIP,'puerto':unPuerto}
    Posteriormente escribe en el label correspondiente su estado para métricas.
    """
    metrica_isalive = 0
    try:
        url_request = get_url(equipo["ip"], equipo["puerto"]) + 'isalive'
        r = requests.get(url=url_request, timeout=3)
        logging.debug('Código:' + str(r.status_code) + ' Respuesta:' + str(r.json()))
        if r.status_code == 200 and r.json() == True:
            metrica_isalive = 1
            ok.labels(host=equipo["ip"], puerto=equipo["puerto"]).inc()
    except requests.exceptions.HTTPError as errh:
        logging.error("Http Error:" + str(errh))
        fail.labels(host=equipo["ip"], puerto=equipo["puerto"]).inc()
    except requests.exceptions.ConnectionError as errc:
        logging.error("Error Conexión:" + str(errc))
        fail.labels(host=equipo["ip"], puerto=equipo["puerto"]).inc()
    except requests.exceptions.Timeout as errt:
        logging.error("Timeout Error:" + str(errt))
        fail.labels(host=equipo["ip"], puerto=equipo["puerto"]).inc()
    except requests.exceptions.RequestException as err:
        logging.error("Otro Error: " + str(err))
        fail.labels(host=equipo["ip"], puerto=equipo["puerto"]).inc()
    logging.info('URL: ' + url_request + ' Estado:' + str(metrica_isalive))
    status.labels(host=equipo["ip"], puerto=equipo["puerto"]).set(metrica_isalive)


def get_secciones(archivo):
    """
    Obtiene las secciones y a su vez sus atributos a partir de un archivo de configuración.
    :param archivo: Str -> ruta
    :return: Str-> ip, Int-> puerto, Str-> url
    """
    try:
        configuracion = configparser.ConfigParser()
        configuracion.read(archivo)
        seccionInit = configuracion['INIT']
        seccionURL = configuracion['URL']
        ip = seccionInit['IP']
        try:
            ip = ip[1:-1]  # quitando comillas
        except Exception as e:
            logging.error("ERROR - obtención ip - "+ str(e))
        puerto = seccionInit['PUERTO']
        url = seccionURL['URL']
        try:
            url = url[1:-1]  # quitando comillas
        except Exception as e:
            logging.error("ERROR - obtención url - "+ str(e))
    except KeyError as e:
        logging.error("ERROR -  Seccion no encontrada - "+ str(e))
    except Exception as e:
        logging.error("ERROR - Abrir archivo - "+ str(e))
    logging.debug(ip + puerto + url)
    return ip, puerto, url


def existe_archivo(archivo):
    """
    Verifica si se encuentra en directorio el archivo
    :param archivo: Str -> ruta
    :return: Boolean
    """
    if os.path.exists(archivo):
        logging.info("Archivo metric_simono.conf EXISTE! ")
        return True
    logging.info("Archivo metric_simono.conf NO EXISTE! ")
    return False


def crear_archivo_default():
    """
    Encargado de construir un archivo de configuración base con sus secciones y atributos seteados por default
    """
    logging.info("Creando archivo de configuración base...")
    archivo = open('metric_simono.conf', 'w')
    archivo.write("[INIT]\n")
    archivo.write("# IP Y PUERTO de donde se va a iniciar el servicio\n")
    archivo.write("IP = '127.0.0.1'\n")
    archivo.write("PUERTO = 5000\n")
    archivo.write("\n")
    archivo.write("\n")
    archivo.write("[URL]\n")
    archivo.write("# ruta a donde se va a pedir los equipos que se va a consultar\n")
    archivo.write("URL = 'http://127.0.0.1:8000/Api/getEquipos/'")
    archivo.close()


def get_equipos(url):
    """
    Solicita los equipos registrados a monitorear a partir de una URL que se pasa por parámetro y obtiene una lista de
    equipos con su IP y puerto.
    :param url: Str -> url
    :return: List -> [{'ip':unaIp,'puerto':unPuerto}]
    """
    equipos = []
    try:
        logging.info("Solicitando Equipos...")
        r = requests.get(url=url, timeout=5)
        logging.info("EQUIPOS -> " + str(r.json()))
        if r.status_code == 200:
            data = r.json()
            equipos = data['equipos']
    except requests.exceptions.Timeout as e:
        logging.error("ERROR - solicitud timeout - " + str(e))
    except Exception as e:
        logging.error("ERROR - " + str(e))
    return equipos
