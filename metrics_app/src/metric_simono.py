#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from flask import Flask
from prometheus_client import generate_latest, REGISTRY
from utils import get_status, get_secciones, get_equipos, crear_archivo_default, existe_archivo

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

app = Flask(__name__)


archivo = 'metric_simono.conf'
if not existe_archivo(archivo):
    crear_archivo_default()
ip, puerto, url = get_secciones(archivo)
equipos = get_equipos(url)


######### Metricas ########
@app.route('/metrics')
def metrics():
    for equipo in equipos:
        get_status(equipo)
    return generate_latest(REGISTRY)


if __name__ == "__main__":
    logging.info("Inciando servicio en:"+ ip +":"+ puerto)
    app.run(host=ip, port=puerto)
